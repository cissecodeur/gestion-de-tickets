package org.sid.entities;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;


@SuppressWarnings("serial")
@Entity
public class Categorie implements Serializable{
	
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(length = 75)
	private String nom;
	
	@OneToMany(mappedBy = "categorie")
	@JsonProperty(access = Access.READ_WRITE)
	private Collection<Film> films;

	public Categorie() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Categorie(int id, String nom, Collection<Film> films) {
		super();
		this.id = id;
		this.nom = nom;
		this.films = films;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Collection<Film> getFilms() {
		return films;
	}

	public void setFilms(Collection<Film> films) {
		this.films = films;
	}
	
	

}

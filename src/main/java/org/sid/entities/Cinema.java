package org.sid.entities;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@SuppressWarnings("serial")
@Entity
public class Cinema implements Serializable{

	 @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	 private Long id;
	 @Column(length = 75)
	 private String nom;
	 private double nombreSalle;
	 public double latitude,longitude,altitude;
	 @OneToMany(mappedBy = "cinema")
	 private Collection<Salle> salles;
	 @ManyToOne
	 private Ville ville;
	 
	 public Cinema() {
			super();
			// TODO Auto-generated constructor stub
		}

	public Cinema(Long id, String nom, double nombreSalle, double latitude, double longitude, double altitude,
			Collection<Salle> salles, Ville ville) {
		super();
		this.id = id;
		this.nom = nom;
		this.nombreSalle = nombreSalle;
		this.latitude = latitude;
		this.longitude = longitude;
		this.altitude = altitude;
		this.salles = salles;
		this.ville = ville;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public double getNombreSalle() {
		return nombreSalle;
	}

	public void setNombreSalle(double nombreSalle) {
		this.nombreSalle = nombreSalle;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getAltitude() {
		return altitude;
	}

	public void setAltitude(double altitude) {
		this.altitude = altitude;
	}

	public Collection<Salle> getSalles() {
		return salles;
	}

	public void setSalles(Collection<Salle> salles) {
		this.salles = salles;
	}

	public Ville getVille() {
		return ville;
	}

	public void setVille(Ville ville) {
		this.ville = ville;
	}
	 
}

package org.sid.entities;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@SuppressWarnings("serial")
@Entity
public class Salle implements Serializable {
	
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(length = 75)
	private String nom;
	private double nombrePlace;
	@OneToMany(mappedBy = "salle")
	private Collection<Place> places;
	@OneToMany(mappedBy = "salle")
	private Collection<Projection> projections;
	@ManyToOne
	private Cinema cinema;
	
	public Salle() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Salle(Long id, String nom, double nombrePlace, Collection<Place> places, Collection<Projection> projections,
			Cinema cinema) {
		super();
		this.id = id;
		this.nom = nom;
		this.nombrePlace = nombrePlace;
		this.places = places;
		this.projections = projections;
		this.cinema = cinema;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public double getNombrePlace() {
		return nombrePlace;
	}
	public void setNombrePlace(double nombrePlace) {
		this.nombrePlace = nombrePlace;
	}
	public Collection<Place> getPlaces() {
		return places;
	}
	public void setPlaces(Collection<Place> places) {
		this.places = places;
	}
	public Collection<Projection> getProjections() {
		return projections;
	}
	public void setProjections(Collection<Projection> projections) {
		this.projections = projections;
	}
	public Cinema getCinema() {
		return cinema;
	}
	public void setCinema(Cinema cinema) {
		this.cinema = cinema;
	}
 
	 
	 
}

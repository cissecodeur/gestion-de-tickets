package org.sid.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@SuppressWarnings("serial")
@Entity
public class Seance implements Serializable {

	 @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	 private Long id;
	 
	 @Temporal(TemporalType.TIME)
	 private Date heureDebut;
	 @OneToMany(mappedBy = "seance")
	 Collection<Projection> projections;
	 
	public Seance() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Seance(Long id, Date heureDebut, Collection<Projection> projections) {
		super();
		this.id = id;
		this.heureDebut = heureDebut;
		this.projections = projections;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getHeureDebut() {
		return heureDebut;
	}

	public void setHeureDebut(Date heureDebut) {
		this.heureDebut = heureDebut;
	}

	public Collection<Projection> getProjections() {
		return projections;
	}

	public void setProjections(Collection<Projection> projections) {
		this.projections = projections;
	}
	 
}

package org.sid.entities;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;


@SuppressWarnings("serial")
@Entity
public class Ville implements Serializable{

	
	 @Id
	 @GeneratedValue(strategy = GenerationType.IDENTITY)
	 private Long id;
	 @Column(length = 75 )
	 private String nom;
	 private double latitude,longitude,altitude; 
	 @OneToMany(mappedBy = "ville")
	 private Collection<Cinema> cinemas;
	 
	 public Ville() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Ville(Long id, String nom, double latitude, double longitude, double altitude, Collection<Cinema> cinemas) {
		super();
		this.id = id;
		this.nom = nom;
		this.latitude = latitude;
		this.longitude = longitude;
		this.altitude = altitude;
		this.cinemas = cinemas;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getAltitude() {
		return altitude;
	}

	public void setAltitude(double altitude) {
		this.altitude = altitude;
	}

	public Collection<Cinema> getCinemas() {
		return cinemas;
	}

	public void setCinemas(Collection<Cinema> cinemas) {
		this.cinemas = cinemas;
	}
	 
	 
	 
	 
}

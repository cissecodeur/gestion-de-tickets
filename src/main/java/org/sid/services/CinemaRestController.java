package org.sid.services;


import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.sid.dao.FilmRepository;
import org.sid.dao.TicketRepository;
import org.sid.entities.Film;
import org.sid.entities.Ticket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.*;

@RestController
public class CinemaRestController {
	
	@Autowired
	private FilmRepository filmrepository;
	private TicketRepository ticketrepository;
	
	
	@GetMapping(path = "/ImagesFilms/{id}",produces = MediaType.IMAGE_JPEG_VALUE)
	public byte [] AfficherImage(@PathVariable(name="id") Long id) throws Exception {
		Film film = filmrepository.findById(id).get();
		String photoName =film.getPhoto();
		File file = new File(System.getProperty("user.home")+"Images/"+photoName+".JPG") ;
		Path path = Paths.get(file.toURI());
				return Files.readAllBytes(path);
		
	}
	
	
	@PostMapping("/acheterTicket")
	public List<Ticket> achaTicket(@RequestBody Tycketform ticketform) {
		List<Ticket> listickets = new ArrayList<Ticket>();
		ticketform.getTickets().forEach(idTicket->{
			Ticket ticket = ticketrepository.findById(idTicket).get();
			ticket.setNomClient(ticketform.getNomClient());
			ticket.setReserve(true);
			ticketrepository.save(ticket);
			listickets.add(ticket);
		});
		
		return listickets;
	}
	
	class Tycketform {
		
		private String nomClient;
		private List<Long> tickets = new ArrayList<Long>();
		
		public Tycketform() {
			super();
			// TODO Auto-generated constructor stub
		}

		public Tycketform(String nomClient, List<Long> tickets) {
			super();
			this.nomClient = nomClient;
			this.tickets = tickets;
		}

		public String getNomClient() {
			return nomClient;
		}

		public void setNomClient(String nomClient) {
			this.nomClient = nomClient;
		}

		public List<Long> getTickets() {
			return tickets;
		}

		public void setTickets(List<Long> tickets) {
			this.tickets = tickets;
		}
		
		
	}
}

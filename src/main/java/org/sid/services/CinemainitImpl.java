package org.sid.services;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.stream.Stream;

import javax.transaction.Transactional;

import org.sid.dao.CategorieRepository;
import org.sid.dao.CinemaRepository;
import org.sid.dao.FilmRepository;
import org.sid.dao.PlaceRepository;
import org.sid.dao.ProjectionRepository;
import org.sid.dao.SalleRepository;
import org.sid.dao.SeanceRepository;
import org.sid.dao.TicketRepository;
import org.sid.dao.VilleRepository;
import org.sid.entities.Categorie;
import org.sid.entities.Cinema;
import org.sid.entities.Film;
import org.sid.entities.Place;
import org.sid.entities.Projection;
import org.sid.entities.Salle;
import org.sid.entities.Seance;
import org.sid.entities.Ticket;
import org.sid.entities.Ville;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class CinemainitImpl implements InterfCinemaInitService{

	@Autowired
	private VilleRepository villerepository;
	
	@Autowired
	private CinemaRepository cinemarepository;
	
	@Autowired
	private SalleRepository sallerepository;
	
	@Autowired
	private PlaceRepository placerepository;
	
	@Autowired
	private SeanceRepository seancerepository;
	
	@Autowired
	private CategorieRepository categorierepository;
	
	@Autowired
	private FilmRepository filmrepository;
	
	@Autowired
	private ProjectionRepository projectionrepository;
	
	@Autowired
	private TicketRepository ticketrepository;
	
	
	
	@Override	
	public void initVilles() {
		Stream.of("Abidjan","Bouake","Odienne","Daloa").forEach(nomVille-> {
			Ville ville = new Ville();
			ville.setNom(nomVille);
			villerepository.save(ville);
		});
		
	}

	
	@Override
	public void initCinemas() {
	 villerepository.findAll().forEach(ville->{
			Stream.of("Yacouba Cisse","sodia","Bon fils","Delpi").forEach(nomCinema-> {
				Cinema cinema = new Cinema();
				cinema.setNom(nomCinema);
				cinema.setVille(ville);
				cinema.setNombreSalle(3+ (int)(Math.random()*7));
				cinemarepository.save(cinema);
			});
							
		});
		
	}

	@Override
	public void initSalles() {
		cinemarepository.findAll().forEach(cinema->{
			for(int i=0 ; i<cinema.getNombreSalle(); i++ ) {
				 Salle salle = new Salle();
				 salle.setCinema(cinema);
				 salle.setNom("Salle" + (i+1) );
				 salle.setNombrePlace(15 + (int)(Math.random()*20));
				 sallerepository.save(salle);
			}
		});
		
	}

	@Override
	public void initPlaces() {
		sallerepository.findAll().forEach(salle->{
			for(int i=0;i<salle.getNombrePlace();i++) {
				Place place = new Place();
				place.setNumero(i+1);
				place.setSalle(salle);
				placerepository.save(place);
			
				
			}
		});
		
	}

	@Override
	public void initSeances() {
		
		DateFormat dateformat = new SimpleDateFormat("HH:mm");
		Stream.of("12:00","15:00","17h:00","19h:00","21:00").forEach(s->{
			Seance seance = new Seance();
			try {
				seance.setHeureDebut(dateformat.parse(s));
				seancerepository.save(seance);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		});
		
	}

	@Override
	public void initCategories() {
		Stream.of("Drames" ,"Feuilletons" ,"Western","Actions","Horreurs").forEach(Nomcategories->{
			Categorie categorie = new Categorie();
			categorie.setNom(Nomcategories);
			categorierepository.save(categorie);
		});
		
	}

	@Override
	public void initFilms() {
		
		double [] durees = new double [] {1 , 1.5 ,2 , 3.5}; 
	    List<Categorie> categories = categorierepository.findAll();
	    	Stream.of("Titanic","Romeo must die","les sept mercenaires","Ruby").forEach(Nomfilm->{
				Film film = new Film();
				film.setTitre(Nomfilm);
				film.setPhoto(Nomfilm);
				film.setDuree(durees[new Random().nextInt(durees.length)]);
				film.setCategorie(categories.get(new Random().nextInt(categories.size())));
                filmrepository.save(film);
			});
		
		}

	
	@Override
	public void initProjections() {
		
		double [] prix = new double [] {1000,2000,3000,4000};
		villerepository.findAll().forEach(ville->{
			ville.getCinemas().forEach(cinema->{
				cinema.getSalles().forEach(salle->{
					filmrepository.findAll().forEach(film->{
						 seancerepository.findAll().forEach(seance->{
							 Projection projections = new Projection();
							 projections.setDateProjection(new Date());
							 projections.setFilm(film);
							 projections.setPrix(prix[new Random().nextInt(prix.length)]);
							 projections.setSalle(salle);
							 projections.setSeance(seance);				 
							 projectionrepository.save(projections);
						 });
						
					});
				});
			});
		});
			
		
		
	}

	@Override
	public void initTickets() {
		projectionrepository.findAll().forEach(projection->{
			projection.getSalle().getPlaces().forEach(place->{
				
				Ticket ticket = new Ticket();
				ticket.setPlace(place);
				ticket.setPrix(projection.getPrix());
				ticket.setProjection(projection);
				ticket.setReserve(false);
				ticketrepository.save(ticket);
			});
		});
		
	}

}
 